package com.lti.lens.Controllers;

import java.util.HashMap;
import java.util.Map;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

// Test
@Controller
@RequestMapping("/charts")
public class ChartController {
	@RequestMapping(value = "/index", method = RequestMethod.GET)
	public String index(ModelMap model) {
		model.addAttribute("message", "Hello AIQ");
		return "charts";

	}

	@RequestMapping(value = "/barchart", method = RequestMethod.GET)
	public String barchart(ModelMap model) {
		Map<String, Integer> barmap = new HashMap();
		barmap.put("Africa", 107);
		barmap.put("America", 31);
		barmap.put("Asia", 635);
		barmap.put("Europe", 203);
		barmap.put("Oceania", 2);
		model.addAttribute("barmap", barmap);
		return "barchart";

	}

	@RequestMapping(value = "/dashboard", method = RequestMethod.GET)
	public String dashboard(ModelMap model) {
		model.addAttribute("message", "Hello AIQ");
		return "dashboard";

	}

	@RequestMapping(value = "/linechart", method = RequestMethod.GET)
	public String linechart(ModelMap model) {
		model.addAttribute("message", "Hello AIQ");
		return "linechart";

	}

	@RequestMapping(value = "/piechart", method = RequestMethod.GET)
	public String piechart(ModelMap model) {
		model.addAttribute("message", "Hello AIQ");
		return "piechart";

	}

}
