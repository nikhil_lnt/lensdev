<html>
<head>
<title>Highcharts Tutorial</title>
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js">
      </script>
<script src="https://code.highcharts.com/highcharts.js"></script>
</head>

<body>
	<div id="container" style="width: 550px; height: 400px; margin: 0 auto"></div>
	<script language="JavaScript">
      	var bar_map = '${barmap}';
      	bar_map = bar_map.replace("{", "")
      	bar_map = bar_map.replace("}", "")
      	//console.log(bar_map);
      	bar_arr = bar_map.split(',');
      	var cntry = [];
      	var population = [];
      	for (  i = 0 ; i< bar_arr.length; i++){
      		var temp = bar_arr[i].split("=");
      		cntry.push(temp[0]);
      		population.push(parseInt(temp[1]))
      			
      	};
      	console.log(population);
      	$(document).ready(function() {  
            var chart = {
               type: 'bar'
            };
            var title = {
               text: 'Historic World Population by Region'   
            };
            var subtitle = {
               text: 'Source: Lens R&D'  
            };
            var xAxis = {
               categories: cntry,
               title: {
                  text: null
               }
            };
            var yAxis = {
               min: 0,
               title: {
                  text: 'Population (millions)',
                  align: 'high'
               },
               labels: {
                  overflow: 'justify'
               }
            };
            var tooltip = {
               valueSuffix: ' millions'
            };
            var plotOptions = {
               bar: {
                  dataLabels: {
                     enabled: true
                  }
               }
            };
            var legend = {
               layout: 'vertical',
               align: 'right',
               verticalAlign: 'top',
               x: -40,
               y: 100,
               floating: true,
               borderWidth: 1,
               
               backgroundColor: (
                  (Highcharts.theme && Highcharts.theme.legendBackgroundColor) ||
                     '#FFFFFF'),
               shadow: true
            };
            var credits = {
               enabled: false
            };
            var series = [
               {
                  name: 'Year 1800',
                  data: population
               }
            ];
      			
            var json = {};   
            json.chart = chart; 
            json.title = title;   
            json.subtitle = subtitle; 
            json.tooltip = tooltip;
            json.xAxis = xAxis;
            json.yAxis = yAxis;  
            json.series = series;
            json.plotOptions = plotOptions;
            json.legend = legend;
            json.credits = credits;
            $('#container').highcharts(json);
         });
      </script>
</body>

</html>