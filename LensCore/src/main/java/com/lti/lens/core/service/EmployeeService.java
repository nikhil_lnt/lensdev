package com.lti.lens.core.service;

import org.springframework.orm.hibernate3.HibernateTemplate;

import com.lti.lens.core.model.Employee;

import java.util.*;

public interface EmployeeService {

//	HibernateTemplate template;
//	public void setTemplate(HibernateTemplate template);

	public int saveEmployee(Employee e);
	public void updateEmployee(Employee e);
	public void deleteEmployee(Employee e) ;
	public Employee getById(int id);
	public List<Employee> getEmployees();
	
	public double getTotalSal();
	public double mySaveUpdateDelete(Employee e);
}